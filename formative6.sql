INSERT INTO brand 
	(name)
VALUES
	('Brand A'), ('Brand B'), ('Brand C'),
    ('Merek A'), ('Merek B'), ('Merek C');

INSERT INTO production.product 
	(name, description, art_number, price, stock, is_deleted, brand_id)
SELECT 
    p.name, p.description, p.artnumber, p.price, p.stock, p.deleted, b.id as brand_id
FROM 
    staging.product_clienta p
JOIN 
    production.brand b ON p.brand_name = b.name;

INSERT INTO production.product 
	(name, description, art_number, price, stock, is_deleted, brand_id)
SELECT 
    p.nama, p.deskripsi, p.nomor_artikel, p.harga, p.persediaan_barang, p.dihapus, b.id as brand_id
FROM 
    staging.product_clientb p
JOIN 
    production.brand b ON p.nama_merek = b.name;
