package id.co.nexsoft.multidatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(
	scanBasePackages= {
		"id.co.nexsoft.config", "id.co.nexsoft.production", "id.co.nexsoft.staging"},
	exclude = {DataSourceAutoConfiguration.class}
)
public class MultidatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultidatabaseApplication.class, args);
	}

}
