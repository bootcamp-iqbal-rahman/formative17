package id.co.nexsoft.multidatabase.config;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.rsocket.RSocketProperties.Server;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    basePackages = "id.co.nexsoft.production.model",
    entityManagerFactoryRef = "productionEntityManagerFactory",
    transactionManagerRef = "productionTransactionManager"
)
public class ProductionDataSourceConfig {
    
    @Bean
    @ConfigurationProperties("spring.datasource.production")
    public DataSourceProperties productionDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties("spring.datasource.production.configuration")
    public DataSource productionDataSource() {
        return productionDataSourceProperties().initializeDataSourceBuilder()
                .type(HikariDataSource.class).build();
    }

    @Bean
    public EntityManagerFactoryBuilder entityManagerFactoryBuilder() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(false);
        return new EntityManagerFactoryBuilder(vendorAdapter, new HashMap<>(), null);
    }

    @Bean(name = "productionEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean productionEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder.dataSource(productionDataSource())
                .packages(Server.class).build();
    }

    @Bean(name = "productionTransactionManager")
    public PlatformTransactionManager productionTransactionManager(
        final @Qualifier("productionEntityManagerFactory")
        LocalContainerEntityManagerFactoryBean productionEntityManagerFactory
    ) {
        return new JpaTransactionManager(productionEntityManagerFactory.getObject());
    }

}
