package id.co.nexsoft.multidatabase.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.rsocket.RSocketProperties.Server;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    basePackages = "id.co.nexsoft.staging.model",
    entityManagerFactoryRef = "stagingEntityManagerFactory",
    transactionManagerRef = "stagingTransactionManager"
)
public class StagingDataSourceConfig {
    
    @Bean
    @ConfigurationProperties("spring.datasource.staging")
    public DataSourceProperties stagingDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties("spring.datasource.staging.configuration")
    public DataSource stagingDataSource() {
        return stagingDataSourceProperties().initializeDataSourceBuilder()
                .type(HikariDataSource.class).build();
    }

    @Bean(name = "stagingEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean stagingEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder.dataSource(stagingDataSource())
                .packages(Server.class).build();
    }

    @Bean(name = "stagingTransactionManager")
    public PlatformTransactionManager stagingTransactionManager(
        final @Qualifier("stagingEntityManagerFactory")
        LocalContainerEntityManagerFactoryBean stagingEntityManagerFactory
    ) {
        return new JpaTransactionManager(stagingEntityManagerFactory.getObject());
    }

}
