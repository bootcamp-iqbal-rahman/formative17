package id.co.nexsoft.multidatabase.production.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.multidatabase.production.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    List<Product> findAll();
}