package id.co.nexsoft.multidatabase.staging.service;

import java.util.List;

public interface StagingService<T> {
    T setConn(T data);
    List<T> getProducts();
}
