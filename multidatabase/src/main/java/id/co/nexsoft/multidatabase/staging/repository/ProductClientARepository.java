package id.co.nexsoft.multidatabase.staging.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.multidatabase.staging.model.ProductClientA;

public interface ProductClientARepository extends JpaRepository<ProductClientA, Integer> {
    
}