package id.co.nexsoft.multidatabase.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.multidatabase.production.model.Product;
import id.co.nexsoft.multidatabase.production.repository.ProductRepository;
import id.co.nexsoft.multidatabase.production.service.ProductionService;

@Service
public class ProductServiceImpl implements ProductionService<Product> {
    
    @Autowired
    private ProductRepository repository;
    
    @Override
    public Product setConn(Product data) {
        return repository.save(data);
    }

    @Override
    public List<Product> getProducts() {
        return repository.findAll();
    }
    
}
