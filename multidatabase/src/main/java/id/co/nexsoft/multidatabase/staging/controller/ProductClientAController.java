package id.co.nexsoft.multidatabase.staging.controller;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import id.co.nexsoft.multidatabase.staging.model.ProductClientA;
import id.co.nexsoft.multidatabase.staging.service.StagingService;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
@RequestMapping("/staging/product/a")
public class ProductClientAController {
    
    @Autowired
    private StagingService<ProductClientA> stagingService;

    @GetMapping
    public List<ProductClientA> getProducts() {
        return stagingService.getProducts();
    }
    
    @PostMapping
    public ResponseEntity<String> setConn(@RequestBody ProductClientA data) {
        if (!ObjectUtils.isEmpty(stagingService.setConn(data))) {
            return new ResponseEntity<>("Staging - success", HttpStatus.OK);
        }
        return new ResponseEntity<>("Staging - error", HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
}
