package id.co.nexsoft.multidatabase.production.service;

import java.util.List;

public interface ProductionService<T> {
    T setConn(T data);
    List<T> getProducts();
}
