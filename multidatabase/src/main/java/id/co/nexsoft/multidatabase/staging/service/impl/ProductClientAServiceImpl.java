package id.co.nexsoft.multidatabase.staging.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.multidatabase.staging.model.ProductClientA;
import id.co.nexsoft.multidatabase.staging.repository.ProductClientARepository;
import id.co.nexsoft.multidatabase.staging.service.StagingService;

@Service
public class ProductClientAServiceImpl implements StagingService<ProductClientA> {
    
    @Autowired
    private ProductClientARepository repository;

    @Override
    public ProductClientA setConn(ProductClientA data) {
        return repository.save(data);
    }

    @Override
    public List<ProductClientA> getProducts() {
        return repository.findAll();
    }
}
