package id.co.nexsoft.multidatabase.production.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.multidatabase.production.model.Product;
import id.co.nexsoft.multidatabase.production.service.ProductionService;

@RestController
@RequestMapping("/production/product")
public class ProductController {
    
    @Autowired
    private ProductionService<Product> productionService;

    @GetMapping
    public List<Product> getProducts() {
        return productionService.getProducts();
    }

    @PostMapping
    public ResponseEntity<String> setConn(@RequestBody Product data) {
        if (!ObjectUtils.isEmpty(productionService.setConn(data))) {
            return new ResponseEntity<>("Production - success", HttpStatus.OK);
        }
        return new ResponseEntity<>("Production - error", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
