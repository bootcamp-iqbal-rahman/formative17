CREATE TABLE product_clienta (
  id int PRIMARY KEY AUTO_INCREMENT,
  name varchar(50),
  description varchar(255),
  artnumber varchar(20),
  price double,
  stock int,
  deleted boolean,
  brand_name varchar(20)
);

CREATE TABLE product_clientb (
  id int PRIMARY KEY AUTO_INCREMENT,
  nama varchar(50),
  deskripsi varchar(255),
  nomor_artikel varchar(20),
  harga double,
  persediaan_barang int,
  dihapus boolean,
  nama_merek varchar(20)
);

CREATE TABLE product (
  id int PRIMARY KEY AUTO_INCREMENT,
  name varchar(50),
  description varchar(255),
  art_number varchar(25),
  price double,
  stock int,
  is_deleted boolean,
  brand varchar(20)
);