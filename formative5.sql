CREATE TABLE brand (
  id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name varchar(50)
);

ALTER TABLE product
CHANGE brand brand_id int;