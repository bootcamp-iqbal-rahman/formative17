INSERT INTO product_clienta 
	(name, description, artnumber, price, stock, deleted, brand_name)
VALUES 
  ('Product 1', 'Description 1', 'ART001', 19.99, 100, false, 'Brand A'),
  ('Product 2', 'Description 2', 'ART002', 29.99, 150, false, 'Brand B'),
  ('Product 3', 'Description 3', 'ART003', 39.99, 120, false, 'Brand C'),
  ('Product 4', 'Description 4', 'ART004', 49.99, 80, false, 'Brand A'),
  ('Product 5', 'Description 5', 'ART005', 59.99, 200, false, 'Brand B'),
  ('Product 6', 'Description 6', 'ART006', 69.99, 90, false, 'Brand C'),
  ('Product 7', 'Description 7', 'ART007', 79.99, 110, false, 'Brand A'),
  ('Product 8', 'Description 8', 'ART008', 89.99, 70, false, 'Brand B'),
  ('Product 9', 'Description 9', 'ART009', 99.99, 180, false, 'Brand C'),
  ('Product 10', 'Description 10', 'ART010', 109.99, 130, false, 'Brand A')
;

INSERT INTO product_clientb 
	(nama, deskripsi, nomor_artikel, harga, persediaan_barang, dihapus, nama_merek)
VALUES 
  ('Produk 1', 'Deskripsi 1', 'ART001', 19.99, 100, false, 'Merek A'),
  ('Produk 2', 'Deskripsi 2', 'ART002', 29.99, 150, false, 'Merek B'),
  ('Produk 3', 'Deskripsi 3', 'ART003', 39.99, 120, false, 'Merek C'),
  ('Produk 4', 'Deskripsi 4', 'ART004', 49.99, 80, false, 'Merek A'),
  ('Produk 5', 'Deskripsi 5', 'ART005', 59.99, 200, false, 'Merek B'),
  ('Produk 6', 'Deskripsi 6', 'ART006', 69.99, 90, false, 'Merek C'),
  ('Produk 7', 'Deskripsi 7', 'ART007', 79.99, 110, false, 'Merek A'),
  ('Produk 8', 'Deskripsi 8', 'ART008', 89.99, 70, false, 'Merek B'),
  ('Produk 9', 'Deskripsi 9', 'ART009', 99.99, 180, false, 'Merek C'),
  ('Produk 10', 'Deskripsi 10', 'ART010', 109.99, 130, false, 'Merek A')
;
