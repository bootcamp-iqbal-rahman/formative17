INSERT INTO production.product 
	(name, description, art_number, price, stock, is_deleted, brand)
SELECT 
	name, description, artnumber, price, stock, deleted, brand_name 
FROM 
	staging.product_clienta;

INSERT INTO production.product 
	(name, description, art_number, price, stock, is_deleted, brand)
SELECT 
	nama, deskripsi, nomor_artikel, harga, persediaan_barang, dihapus, nama_merek 
FROM 
	staging.product_clientb;
